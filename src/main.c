#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
#define BLOCK_MIN_CAPACITY 24
#define INITIAL_SIZE 4096
#define BLOCK_SIZE_1 10
#define BLOCK_SIZE_2 200
#define BLOCK_SIZE_3 50

static void test_successful_allocation() {
    const void *heap = heap_init(1);
    const struct block_header *head = heap;

    assert(size_from_capacity(head->capacity).bytes == REGION_MIN_SIZE);
    assert(head->is_free);
    assert(head->next == NULL);

    void *allocated_memory = _malloc(BLOCK_SIZE_1);
    assert(allocated_memory != NULL && "Failed to allocate memory");

    assert(head->capacity.bytes == BLOCK_MIN_CAPACITY);
    assert(!head->is_free);

    heap_term();
}

static void test_free_single_block() {
    const void *heap = heap_init(1);
    const struct block_header *head = heap;

    assert(size_from_capacity(head->capacity).bytes == REGION_MIN_SIZE);
    assert(head->is_free);
    assert(head->next == NULL);

    void *allocated_memory = _malloc(BLOCK_SIZE_1);
    assert(allocated_memory != NULL && "Failed to allocate memory");

    assert(head->capacity.bytes == BLOCK_MIN_CAPACITY);
    assert(!head->is_free);

    _free(allocated_memory);
    assert(head->is_free);

    heap_term();
}

static void test_free_multiple_blocks() {
    const void *heap = heap_init(1);
    const struct block_header *head = heap;

    assert(size_from_capacity(head->capacity).bytes == REGION_MIN_SIZE);
    assert(head->is_free);
    assert(head->next == NULL);

    void *allocated_memory1 = _malloc(BLOCK_SIZE_1);
    assert(allocated_memory1 != NULL && "Failed to allocate memory");

    assert(head->capacity.bytes == BLOCK_MIN_CAPACITY);
    assert(!head->is_free);

    void *allocated_memory2 = _malloc(BLOCK_SIZE_2);
    assert(allocated_memory2 != NULL && "Failed to allocate memory");

    assert(head->next != NULL);
    assert(!head->next->is_free);

    void *allocated_memory3 = _malloc(BLOCK_SIZE_3);
    assert(allocated_memory3 != NULL && "Failed to allocate memory");

    assert(head->next->next != NULL);
    assert(!head->next->next->is_free);
    assert(head->next->next->next != NULL);

    _free(allocated_memory2);
    assert(head->next->is_free);

    assert(head->next->next->next != NULL);

    _free(allocated_memory1);
    assert(head->is_free);

    assert(head->next->next->next == NULL);

    assert(head->next->next != NULL);

    _free(allocated_memory3);
    assert(head->next->is_free);

    assert(head->next->next == NULL);

    heap_term();
}

static void test_out_of_memory_expansion() {
    const void *heap_start = heap_init(INITIAL_SIZE);

    assert(heap_start != NULL && "Failed to initialize heap");

    void *allocated_memory1 = _malloc(BLOCK_SIZE_1);

    assert(allocated_memory1 != NULL && "Failed to allocate memory");

    void *allocated_memory2 = _malloc(INITIAL_SIZE - BLOCK_SIZE_1);

    assert(allocated_memory2 != NULL && "Failed to allocate memory");

    void *allocated_memory3 = _malloc(BLOCK_SIZE_1);

    assert(allocated_memory3 != NULL && "Failed to allocate memory");

    heap_term();
}

#define abs_(x) ((x) < 0 ? -(x) : (x))

static void test_out_of_memory_no_expansion() {
    const void *heap_start = heap_init(INITIAL_SIZE);

    assert(heap_start != NULL && "Failed to initialize heap");

    void *allocated_memory1 = _malloc(BLOCK_SIZE_1);

    assert(allocated_memory1 != NULL && "Failed to allocate memory");

    void *allocated_memory2 = _malloc(INITIAL_SIZE - BLOCK_SIZE_1);

    assert(allocated_memory2 != NULL && "Failed to allocate memory");

    void *dummy =
        mmap((void *)heap_start + size_max(INITIAL_SIZE, REGION_MIN_SIZE),
             INITIAL_SIZE, PROT_READ | PROT_WRITE,
             MAP_FIXED_NOREPLACE | MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);

    assert(dummy != MAP_FAILED && "Failed to allocate memory");

    void *allocated_memory3 = _malloc(INITIAL_SIZE);

    assert(abs_(allocated_memory3 - allocated_memory2) >
               size_max(INITIAL_SIZE, REGION_MIN_SIZE) &&
           "Memory seems to be allocates consequently");

    munmap(dummy, INITIAL_SIZE);
    heap_term();
}

int main() {
    test_successful_allocation();
    test_free_single_block();
    test_free_multiple_blocks();
    test_out_of_memory_expansion();
    test_out_of_memory_no_expansion();

    return 0;
}
